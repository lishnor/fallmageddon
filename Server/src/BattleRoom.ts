import { Room, Client } from "colyseus";
import { RoomState, Player, Team } from "./RoomState";

export class BattleRoom extends Room<RoomState> {

  teamsNumber: number = 2
  spawnPointsCount: number = 4
  teams: Array<Array<Player>> = []
  freeSpawnPoints: Array<boolean> = []
  playerSpawnPoints: Map<string, number> = new Map<string, number>()
  teamsAlive: number
  clientsById: Map<string, Client> = new Map<string, Client>()

  onCreate (options: any) {
    if (options.private) {
      console.log("private");
    }
    this.setState(new RoomState());
    this.maxClients = 2
    this.teamsAlive = this.teamsNumber;
    for (var i = 0; i < this.teamsNumber; ++i) {
      this.teams.push([]);
      const team = new Team()
      team.playersCount = 0;
      team.playersAliveCount = 0;
      team.score = 0;
      this.state.teams[i] = team;
    }
    this.resetSpawnPoints();
    this.onMessage("move", this.onPlayerMove.bind(this));
    this.onMessage("dash", this.onPlayerDash.bind(this));
    this.onMessage("shoot", this.onPlayerShoot.bind(this));
    this.onMessage("hit", this.onPlayerHit.bind(this));
    this.onMessage("die", this.onPlayerDied.bind(this));
    this.onMessage("weapon", this.onPlayerChangedWeapon.bind(this));
  }

  onJoin (client: Client, options: any) {
    const player = new Player();
    player.x = 1000;
    player.y = 1000;
    player.z = 1000;
    player.weaponType = 0;
    player.knockback = 1;
    this.state.players[client.sessionId] = player;
    var i;
    for (i = 0; i < this.teamsNumber; ++i) {
      if (this.teams[i].length < this.maxClients / this.teamsNumber) {
        this.teams[i].push(player);
        const team: Team = this.state.teams[i.toString()];
        team.playersAliveCount++;
        team.playersCount++;
        break;
      }
    }
    player.teamId = i;
    const spawnPoint = this.getFreeSpawnPointId();
    this.playerSpawnPoints.set(client.sessionId, spawnPoint);
    client.send("spawn", { teamId: i, spawnPoint: spawnPoint });
    console.log(`New client ${client.sessionId}`);
    this.clientsById.set(client.sessionId, client)
    if (this.hasReachedMaxClients()) {
      setTimeout(this.unlockPlayers.bind(this), 1000);
      this.lock();
    }
  }

  getFreeSpawnPointId(): number {
    const freeSpawnPointsInd = []
    for(let i = 0; i<this.spawnPointsCount; ++i) {
      if (this.freeSpawnPoints[i]) {
        freeSpawnPointsInd.push(i);
      }
    }
    const chosenInd = Math.floor(Math.random() * freeSpawnPointsInd.length);
    this.freeSpawnPoints[freeSpawnPointsInd[chosenInd]] = false;
    return freeSpawnPointsInd[chosenInd];
  }

  onLeave (client: Client, consented: boolean) {
    this.playerGone(client.sessionId, true);
  }

  onPlayerMove(client: Client, data: any) {
    const player: Player = this.state.players[client.sessionId];
    player.x = data.x;
    player.y = data.y;
    player.z = data.z;
    player.a = data.a;
    player.b = data.b;
    player.c = data.c;
    player.d = data.d;
    player.grounded = data.grounded;
    player.velocity = data.velocity;
  }

  onPlayerDash(client: Client, _data: any) {
    this.broadcast("dash", client.id, { except: client });
  }

  onPlayerShoot(client: Client, data: any) {
    this.broadcast("shoot", {
      clientId: client.id,
      a: data.a,
      b: data.b,
      c: data.c,
      d: data.d,
    }, { except: client });
  }

  onPlayerHit(client: Client, data: any) {
    const player: Player = this.state.players[client.sessionId];
    player.knockback += data.damage;
    this.broadcast("hit", { ...data, damage: player.knockback }, { except: client })
  }

  onPlayerDied(client: Client, _data: any) {
    this.playerGone(client.sessionId, false);
  }

  playerGone(sessionId: string, disconnected: boolean) {
    const player: Player = this.state.players[sessionId];
    const playerTeam: Team = this.state.teams[player.teamId];
    if(disconnected) {
      --playerTeam.playersCount;
      delete this.state.players[sessionId];
      this.freeSpawnPoints[this.playerSpawnPoints.get(sessionId)] = true;
      for (let i = 0; i < this.teamsNumber; ++i) {
        for(let j = 0; j < this.teams[i].length; ++j)
        {
          if (this.teams[i][j] === player) {
            this.teams[i].splice(j, 1);
          }
        }
      }
      this.clientsById.delete(sessionId);
    }
    if(--playerTeam.playersAliveCount <= 0) {
      if(--this.teamsAlive == 1) {
        this.gameOver();
      }
    }
  }

  findWinningTeamAndIncreaseScore() {
    for (let key in this.state.teams) {
      const team: Team = this.state.teams[key];
      if (team.playersAliveCount > 0) {
        ++team.score;
        return parseInt(key, 10);
      }
    }
    return 0;
  }

  gameOver() {
    const winningTeamId = this.findWinningTeamAndIncreaseScore();
    this.broadcast("gameover", winningTeamId);
    if (!this.hasReachedMaxClients()) {
      this.unlock();
    }
    setTimeout(this.roundInit.bind(this), 4000);
  }

  onPlayerChangedWeapon(client: Client, data: any) {
    const player: Player = this.state.players[client.sessionId];
    player.weaponType = data.weaponType;
  }

  roundInit() {
    this.teamsAlive = this.teamsNumber;
    for (let key in this.state.teams) {
      const team: Team = this.state.teams[key];
      team.playersAliveCount = team.playersCount;
    }
    this.resetSpawnPoints();
    for (let key in this.state.players) {
      const player: Player = this.state.players[key];
      player.weaponType = 0;
      player.knockback = 1;
      const spawnPoint = this.getFreeSpawnPointId();
      this.playerSpawnPoints.set(key, spawnPoint);
      this.clientsById.get(key).send("spawn", { teamId: player.teamId, spawnPoint: spawnPoint });
    }
    if (this.hasReachedMaxClients()) {
      this.lock();
      setTimeout(this.unlockPlayers.bind(this), 2000);
    }
  }

  resetSpawnPoints() {
    for (var i = 0; i < this.spawnPointsCount; ++i) {
      this.freeSpawnPoints[i] = true;
    }
  }

  unlockPlayers() {
    this.broadcast("start", "");
    console.log(`Game starting`);
  }

}
