import { Schema, MapSchema, type } from "@colyseus/schema";

export class Player extends Schema {
    @type("number")
    x: number;

    @type("number")
	y: number;
	
    @type("number")
	z: number;
	
    @type("number")
	a: number;
	
    @type("number")
	b: number;
	
    @type("number")
	c: number;
	
    @type("number")
	d: number;
	
    @type("int32")
	teamId: number;

    @type("boolean")
	grounded: number;

    @type("number")
    velocity: number;
    
    @type("int32")
    weaponType: number;

    @type("int32")
    powerupType: number;
	
    @type("number")
	knockback: number;
}

export class Team extends Schema {
    @type("int32")
	playersCount: number;
	
    @type("int32")
	playersAliveCount: number;
	
    @type("int32")
	score: number;
}

export class RoomState extends Schema {
    @type({ map: Player })
    players = new MapSchema<Player>();

    @type({ map: Team })
    teams = new MapSchema<Team>();
}
