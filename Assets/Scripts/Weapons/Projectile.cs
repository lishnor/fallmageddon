﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : PoolObject
{
    [SerializeField] Rigidbody rb;
    [SerializeField] GameObject particleImpact;
    [SerializeField] TrailRenderer trailRenderer;
    [SerializeField] float knocbackDamage = 1f;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Enemy"))
        {
            var player = collision.collider.GetComponent<EnemyNetworking>();
            var hitDirection = player.transform.position - transform.position;
            hitDirection.y = 0;
            hitDirection.Normalize();
            var projectileDirection = transform.forward;
            projectileDirection.y = 0;
            hitDirection.Normalize();
            projectileDirection.y = 0.5f;
            player.GetHit(knocbackDamage, 0.5f * hitDirection + projectileDirection);
            // player.body.AddForce((player.transform.position - transform.position) * player.KnockbackHealth, ForceMode.Impulse);
        }
        if (collision.collider.CompareTag("Dummy"))
        {
            var player = collision.collider.GetComponent<PlayerController>();
            var hitDirection = player.transform.position - transform.position;
            hitDirection.y = 0;
            hitDirection.Normalize();
            var projectileDirection = transform.forward;
            projectileDirection.y = 0;
            hitDirection.Normalize();
            projectileDirection.y = 0.5f;
            player.GetHit(knocbackDamage);
            player.body.AddForce((0.5f * hitDirection + projectileDirection) * player.KnockbackHealth, ForceMode.Impulse);
        }

        ContactPoint contactPoint = collision.GetContact(0);

        PoolManager.instance.ReuseObject(particleImpact,contactPoint.point,Quaternion.Euler(contactPoint.normal));
        Destroy();
    }

    void CalculateDirection()
    {

    }

    public override void OnObjectReuse()
    {
        trailRenderer.Clear();
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

        rb.AddForce(transform.forward * 15f, ForceMode.Impulse);
    }
}
