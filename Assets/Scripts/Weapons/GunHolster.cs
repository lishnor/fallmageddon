﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GunHolster : MonoBehaviour
{
    private int _activeGunId = -1;
    public int activeGunId { get { return _activeGunId; }
        set {
            if(_activeGunId != value)
            {
                _activeGunId = value;
                SetActiveGun(_activeGunId);
            }
        }
    }
    [HideInInspector] public Gun currentGun;

    public List<Gun> handledGuns = new List<Gun>();

    private void Start()
    {
        GetGuns();
        activeGunId = 0;
    }

    private void SetActiveGun(int gunId) 
    {
        if (gunId < handledGuns.Count)
            currentGun = handledGuns[gunId];
        else
            currentGun = handledGuns.FirstOrDefault();
        handledGuns.ForEach(g => g.gameObject.SetActive(false));

        currentGun?.gameObject.SetActive(true);
    }

    [ContextMenu("GetGuns")]
    public void GetGuns()
    {
        GetComponentsInChildren(true, handledGuns);
    }
}
