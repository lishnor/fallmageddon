﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleImpact : PoolObject
{
    [SerializeField] ParticleSystem system;

    public override void OnObjectReuse()
    {
        system.Play();
    }

    private void OnDisable()
    {
        Destroy();
    }

}
