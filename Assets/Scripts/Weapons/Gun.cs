﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public Transform rArmHold;
    public Transform lArmHold;
    //[HideInInspector]public Vector3 positionToLookAt;

    [SerializeField] Transform firePoint;
    [SerializeField] GameObject projectile;

    private void Start()
    {
        PoolManager.instance.CreatePool(projectile, 10);
    }

    public void Aim(Vector3 aimingTarget) 
    {
        transform.LookAt(aimingTarget);
    }

    public void Shoot(Quaternion? rotation) 
    {
        PoolManager.instance.ReuseObject(projectile, firePoint.position, rotation ?? firePoint.rotation);
    }

}
