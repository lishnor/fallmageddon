﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour
{
    void Start()
    {
        Cursor.visible = false;
        //Cursor.lockState = CursorLockMode.Confined;
    }

    void Update()
    {
        transform.position = Input.mousePosition;
    }

    void OnDestroy()
    {
        Cursor.visible = true;
    }
}
