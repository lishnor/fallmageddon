﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class MenuManager : MonoBehaviour
{
    public Slider gameVolumeSlider;
    public InputField addressField;

    void Awake()
    {
        addressField.text = PlayerPrefs.GetString("ServerAddress", "localhost:2567");
        NetworkManager.serverAddress = addressField.text;
    }

    private void Update()
    {
        PlayerPrefs.SetFloat("Game Volume", gameVolumeSlider.value);
    }
    public AudioMixer menuAudioMixer;

    public void PlayRandomLobby()
    {
        NetworkManager.gameMode = GameMode.Random;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void CreatePrivateLobby()
    {
        NetworkManager.gameMode = GameMode.Create;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void PlayPrivateLobby()
    {
        NetworkManager.gameMode = GameMode.Join;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void PlayTutorial()
    {
        NetworkManager.gameMode = GameMode.Tutorial;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void SetMenuVolume(float volume)
    {
        menuAudioMixer.SetFloat("menuVolume", volume);
    }
    public void SetRoomId(string roomId)
    {
        NetworkManager.roomId = roomId;
    }
    public void SetAddress(string address)
    {
        NetworkManager.serverAddress = address;
        PlayerPrefs.SetString("ServerAddress", address);
    }
    public void QuitGame()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
