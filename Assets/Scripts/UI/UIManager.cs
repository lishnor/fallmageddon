﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class UIManager : MonoBehaviour
{
    public GameObject controlInfo;
    public GameObject AreUSure;
    public TextMeshProUGUI LobbyID;
    public GameObject LobbyIDObj;
    public TextMeshProUGUI ScoreRed;
    public TextMeshProUGUI ScoreBlue;
    string roomId;
    void Awake()
    {
        if (NetworkManager.gameMode == GameMode.Tutorial)
        {
            controlInfo.SetActive(true);
            Time.timeScale = 0;
        }
    }
    private void Update()
    {
        if (controlInfo.activeSelf == false && !AreUSure.activeSelf && Input.GetKeyDown("escape"))
        {
            AreUSure.SetActive(true);
        }
        if (controlInfo.activeSelf && Input.GetKeyDown("escape"))
        {
            controlInfo.SetActive(false);
            Time.timeScale = 1;
        }
    }
    public void BackToMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex -1);
    }
    public void Resume()
    {
        Time.timeScale = 1;
    }

    public void SetLobbyId(string id)
    {
        LobbyID.text = "Lobby ID: " + id;
        LobbyIDObj.SetActive(true);
    }

    public void SetTeamInfo(int id, int score, int alive)
    {
        Debug.Log("Team " + id + " " + score + " " + alive);
        if(id == 0)
        {
            ScoreRed.text = score.ToString();
        } else
        {
            ScoreBlue.text = score.ToString();
        }
    }
}
