﻿class SpawnMessage
{
    public int teamId;
    public int spawnPoint;
}

class ShootMessage
{
    public string clientId;
    public float a;
    public float b;
    public float c;
    public float d;
}

class HitMessage
{
    public string clientId;
    public float damage;
    public float x;
    public float y;
    public float z;
}
