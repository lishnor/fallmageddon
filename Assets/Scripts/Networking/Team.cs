// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 0.5.39
// 

using Colyseus.Schema;

public class Team : Schema {
	[Type(0, "int32")]
	public int playersCount = 0;

	[Type(1, "int32")]
	public int playersAliveCount = 0;

	[Type(2, "int32")]
	public int score = 0;
}

