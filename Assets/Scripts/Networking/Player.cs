// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 0.5.39
// 

using Colyseus.Schema;

public class Player : Schema {
	[Type(0, "number")]
	public float x = 0;

	[Type(1, "number")]
	public float y = 0;

	[Type(2, "number")]
	public float z = 0;

	[Type(3, "number")]
	public float a = 0;

	[Type(4, "number")]
	public float b = 0;

	[Type(5, "number")]
	public float c = 0;

	[Type(6, "number")]
	public float d = 0;

	[Type(7, "int32")]
	public int teamId = 0;

	[Type(8, "boolean")]
	public bool grounded = false;

	[Type(9, "number")]
	public float velocity = 0;

	[Type(10, "int32")]
	public int weaponType = 0;

	[Type(11, "int32")]
	public int powerupType = 0;

	[Type(12, "number")]
	public float knockback = 0;
}

