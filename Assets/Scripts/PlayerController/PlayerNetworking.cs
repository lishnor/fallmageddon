﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNetworking : MonoBehaviour
{
    public NetworkManager NetworkManager;
    public PlayerController player;
    public bool isLocked = true;

    public void Shoot(Transform t)
    {
        var rot = t.rotation;
        NetworkManager.Send("shoot", new { a = rot.x, b = rot.y, c = rot.z, d = rot.w });
    }

    public void Move(bool grounded, float velocity)
    {
        var pos = transform.position;
        var rot = transform.rotation;
        NetworkManager.Send("move", new { pos.x, pos.y, pos.z, a = rot.x, b = rot.y, c = rot.z, d = rot.w, grounded, velocity });
    }

    public void Spawn(Transform t)
    {
        transform.position = t.position;
        transform.rotation = t.rotation;
        player.GunHolster.activeGunId = 0;
        if(player.body)
        {
            player.body.velocity = Vector3.zero;
        }
    }

    public void Dash()
    {
        NetworkManager.Send("dash", new { });
    }

    public void GetHit(float damage, Vector3 direction)
    {
        player.GetHit(damage);
        player.body.AddForce(direction * damage, ForceMode.Impulse);
    }

    public void ChangeWeapon(int weaponType)
    {
        NetworkManager.Send("weapon", new { weaponType });
    }

    public void Die()
    {
        NetworkManager.OnPlayerDied(new { });
        isLocked = true;
    }
}
