﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyNetworking : MonoBehaviour
{
    public NetworkManager NetworkManager;
    public string ClientId;
    [SerializeField] GunHolster gunHolster;
    private Animator _anim;
    private IEnumerator _positionCoroutine;
    private IEnumerator _rotationCoroutine;
    private float _lastMoveTime = 0.0f;

    void Start()
    {
        _anim = GetComponent<Animator>();
    }
    
    public void Move(Vector3 position, Quaternion rotation, bool grounded, float velocity)
    {
        if (_positionCoroutine != null)
        {
            StopCoroutine(_positionCoroutine);
        }
        if (_rotationCoroutine != null)
        {
            StopCoroutine(_rotationCoroutine);
        }
        _anim.SetBool("IsGrounded", grounded);
        _anim.SetFloat("Velocity", velocity);

        float animateTime = 0.1f;

        if (_lastMoveTime > 0.0f)
        {
            animateTime = Time.time - _lastMoveTime;
            _lastMoveTime = Time.time;
        }

        // todo: prevent errors from accumulating
        _positionCoroutine = AnimatePosition(transform.position, position, animateTime);
        _rotationCoroutine = AnimateRotation(transform.rotation, rotation, animateTime);
        StartCoroutine(_positionCoroutine);
        StartCoroutine(_rotationCoroutine);
    }

    public void Dash()
    {
        _anim.SetTrigger("Dash");
        FindObjectOfType<AudioManager>().Play("RunningSounds");
    }

    public void Shoot(Quaternion rotation)
    {
        gunHolster.currentGun?.Shoot(rotation);
        FindObjectOfType<AudioManager>().Play("Blast");
    }

    public void GetHit(float damage, Vector3 direction)
    {
        NetworkManager.Send("hit", new { clientId = ClientId, damage, direction.x, direction.y, direction.z });
    }

    public void ChangeWeapon(int weaponType)
    {
        gunHolster.activeGunId = weaponType;
    }

    IEnumerator AnimatePosition(Vector3 origin, Vector3 target, float duration)
    {
        float journey = 0f;
        while (journey <= duration)
        {
            journey = journey + Time.deltaTime;
            float percent = Mathf.Clamp01(journey / duration);

            transform.position = Vector3.Lerp(origin, target, percent);

            yield return null;
        }
    }

    IEnumerator AnimateRotation(Quaternion origin, Quaternion target, float duration)
    {
        float journey = 0f;
        while (journey <= duration)
        {
            journey = journey + Time.deltaTime;
            float percent = Mathf.Clamp01(journey / duration);

            transform.rotation = Quaternion.Slerp(origin, target, percent);

            yield return null;
        }
    }
}
