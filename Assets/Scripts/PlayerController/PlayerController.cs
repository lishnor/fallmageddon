﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(RigidbodyCharacter))]
public class PlayerController : MonoBehaviour
{
    public int TeamId;
    public string Name;

    bool _isDead;
    public bool isDead => _isDead;

    public PlayerNetworking networking;

    //The higer value is the more knocback he recives from projectiles;
    [SerializeField] float knockbackHealth;
    public float KnockbackHealth => knockbackHealth;

    [SerializeField] GunHolster gunHolster;
    public GunHolster GunHolster => gunHolster;

    Rigidbody _body;
    public Rigidbody body => _body;

    public static event Action<PlayerController> OnAnyDie;

    private void Start()
    {
        _body = GetComponent<Rigidbody>();
        FindObjectOfType<AudioManager>().Play("ReadyToRoll");
    }

    public void GetHit(float amount) => knockbackHealth += amount;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Death")) 
        {
            _isDead = true;
            OnAnyDie?.Invoke(this);
            networking?.Die();
            FindObjectOfType<AudioManager>().Play("FallSound");
        }
    }
}
