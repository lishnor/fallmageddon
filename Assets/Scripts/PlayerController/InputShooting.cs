﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputShooting : MonoBehaviour
{
    [SerializeField] GunHolster gunHolster;
    public PlayerNetworking networking;

    private void Update()
    {
        if (networking.isLocked) return;
        if (Input.GetMouseButtonDown(0))
        {
            //RaycastHit _hit;
            //Ray _ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //if (Physics.Raycast(_ray, out _hit))
            //{
            //    gunHolster.currentGun?.Aim(_hit.point);
            //}

            if (gunHolster.currentGun)
            {
                gunHolster.currentGun.Shoot(null);
                FindObjectOfType<AudioManager>().Play("Blast");
                networking.Shoot(gunHolster.currentGun.transform);
            }
        }
    }
}
