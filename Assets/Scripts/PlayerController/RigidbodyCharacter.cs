﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(PlayerNetworking))]
public class RigidbodyCharacter : MonoBehaviour
{
    public float Speed = 5f;
    public float JumpHeight = 2f;
    public float GroundDistance = 0.2f;
    public float DashDistance = 5f;
    public LayerMask Ground;

    private Rigidbody _body;
    private Vector3 _moveInputs = Vector3.zero;
    private Vector3 _lookInputs = Vector3.zero;
    private bool _isGrounded = true;
    private Transform _camera;
    [SerializeField]private Transform _groundChecker;
    private Animator _anim;
    private PlayerNetworking _playerNetworking;

    Vector2 moveInput;
    Vector2 lookInput;
    Vector3 lastKonownRot;

    private void OnEnable()
    {
        //GameObject.FindGameObjectWithTag("GroupTarget").GetComponent<Cinemachine.CinemachineTargetGroup>().AddMember(gameObject.transform,1,1);
    }

    void Start()
    {
        _body = GetComponent<Rigidbody>();
        _anim = GetComponent<Animator>();
        _playerNetworking = GetComponent<PlayerNetworking>();
        _camera = Camera.main.transform;
    }

    void Update()
    {
        _isGrounded = CheckGround();
        _anim.SetBool("IsGrounded", _isGrounded);
        _anim.SetFloat("Velocity", _moveInputs.magnitude);
    }

    void FixedUpdate()
    {
        //Sets DeadZone for move inputs
        _moveInputs = Vector3.zero;
        if (moveInput.magnitude > 0.12)
        {
            _moveInputs.x = moveInput.x;
            _moveInputs.z = moveInput.y;
        }

        _lookInputs.x = lookInput.x;
        _lookInputs.z = lookInput.y;

        if (_lookInputs != Vector3.zero)
        {
            transform.forward = _lookInputs.normalized;
            lastKonownRot = transform.forward;
        }

        else if (_moveInputs != Vector3.zero)
        {
            transform.forward = _moveInputs.normalized;
            lastKonownRot = transform.forward;
        }
        else
            transform.forward = lastKonownRot;

        //Applies gravity
        if (!_isGrounded)
            _moveInputs.y = 3*Physics.gravity.y * Time.deltaTime;

        _moveInputs = Quaternion.Euler(0, _camera.eulerAngles.y, 0) * _moveInputs;
        _body.MovePosition(_body.position + _moveInputs * Speed * Time.fixedDeltaTime);

        //Resets gravity
        _moveInputs.y = 0;

        _playerNetworking.Move(_isGrounded, _moveInputs.magnitude);
    }

    //Gets input value for movement
    public void OnMove(Vector2 value)
    {
        moveInput = value;
    }

    //Gets input value for look direction
    public void OnLook(Vector2 value)
    {
        lookInput = value;
    }

    public void OnJump()
    {
        if(_isGrounded)
        {
            _body.AddForce(Vector3.up * Mathf.Sqrt(JumpHeight * -4f * Physics.gravity.y), ForceMode.VelocityChange);
            FindObjectOfType<AudioManager>().Play("JumpSound");
        }
            
        
    }

    public void OnDash()
    {
        Vector3 dashVelocity = Vector3.Scale(transform.forward, DashDistance * new Vector3((Mathf.Log(1f / (Time.deltaTime * _body.drag + 1)) / -Time.deltaTime), 0, (Mathf.Log(1f / (Time.deltaTime * _body.drag + 1)) / -Time.deltaTime)));
        _body.AddForce(dashVelocity, ForceMode.VelocityChange);
        _anim.SetTrigger("Dash");
        FindObjectOfType<AudioManager>().Play("RunningSounds");
    }

    private bool CheckGround() 
    {
        return Physics.CheckSphere(_groundChecker.position, GroundDistance, Ground, QueryTriggerInteraction.Ignore);
    }
}
