﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class InputMovement : MonoBehaviour
{
    public RigidbodyCharacter character;
    public PlayerNetworking networking;

    private void Update()
    {

        if (!networking.isLocked)
        {
            //Handles Moveinput
            Vector2 move = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            character.OnMove(move);

            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                character.OnDash();
                networking.Dash();
            }
        }

        //Handles LookInput
        Vector2 mousepos = HandleMouseInput();
        mousepos = Vector2.ClampMagnitude(mousepos,1f);
        Vector2 look = new Vector2(mousepos.x, mousepos.y);
        character.OnLook(look);

        if (Input.GetKeyDown(KeyCode.Space)) 
        {
            character.OnJump();
        }
    }
    Vector2 HandleMouseInput() 
    {
        //Retrurns look vector based in ScreenSpace
        //return (Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position));
        
        //Retrurns look vector based in WorldSpace
        RaycastHit _hit;
        Ray _ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(_ray, out _hit))
        {
            return new Vector2(_hit.point.x - transform.position.x, _hit.point.z - transform.position.z);
        }
        return Vector2.zero;
    }
}
