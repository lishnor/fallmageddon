﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    Dictionary<int, Queue<ObjectInstance>> poolDictionary = new Dictionary<int, Queue<ObjectInstance>>();

    static PoolManager _instance;

    public static PoolManager instance 
    {
        get 
        {
            if (_instance == null) 
            {
                _instance = FindObjectOfType<PoolManager>();
            }
            return _instance;
        }
    }

    public void CreatePool(GameObject prefab, int poolSize) 
    {
        int poolKey = prefab.GetInstanceID();

        if (!poolDictionary.ContainsKey(poolKey)) 
        {
            GameObject poolHolder = new GameObject(prefab.name + " pool");
            poolHolder.transform.parent = transform;

            poolDictionary.Add(poolKey, new Queue<ObjectInstance>());
            for (int i = 0; i < poolSize; i++) 
            {
                ObjectInstance newObject = new ObjectInstance(Instantiate(prefab),poolKey);
                poolDictionary[poolKey].Enqueue(newObject);
                newObject.SetParent(poolHolder.transform);
            }
        }
    }

    public void ReuseObject(GameObject prefab, Vector3 position, Quaternion rotation) 
    {
        int poolKey = prefab.GetInstanceID();

        if (poolDictionary.ContainsKey(poolKey))
        {
            ObjectInstance objectToReuse;

            if (poolDictionary[poolKey].Count > 0)
                objectToReuse = poolDictionary[poolKey].Dequeue();
            else
            {
                objectToReuse = new ObjectInstance(Instantiate(prefab), poolKey);
                objectToReuse.SetParent(GameObject.Find(prefab.name + " pool").transform);
            }

            if (objectToReuse.HasPoolObjectScript) EnqueueObject(poolKey, objectToReuse);
            objectToReuse.Reuse(position, rotation);
        }
        else 
        {
            CreatePool(prefab, 5);
            ReuseObject(prefab, position, rotation);
        }
    }

    public void EnqueueObject(PoolObject objectInstance)
    {
        ObjectInstance objectToEnqueque = new ObjectInstance(objectInstance.gameObject, objectInstance.poolKey);
        poolDictionary[objectInstance.poolKey].Enqueue(objectToEnqueque);
    }

    public void EnqueueObject(int poolKey ,ObjectInstance objectInstance) 
    {
        poolDictionary[poolKey].Enqueue(objectInstance);
    }

    public class ObjectInstance 
    {
        int _poolKey;
        GameObject gameObject;
        Transform transform;

        bool hasPoolObjectScript = false;
        public bool HasPoolObjectScript { get; }
        PoolObject poolObjectScript;

        public ObjectInstance(GameObject objectInstance, int poolKey)
        {
            gameObject = objectInstance;
            transform = objectInstance.transform;
            _poolKey = poolKey;

            if (gameObject.GetComponent<PoolObject>()) 
            {
                hasPoolObjectScript = true;
                poolObjectScript = gameObject.GetComponent<PoolObject>();
                poolObjectScript.poolKey = _poolKey;
            }
            gameObject.SetActive(false);
        }

        public void Reuse(Vector3 position, Quaternion rotation)
        {
            gameObject.SetActive(true);
            transform.position = position;
            transform.rotation = rotation;
            if (hasPoolObjectScript) 
            {
                poolObjectScript.OnObjectReuse();
            }
        }

        public void SetParent(Transform parent) 
        {
            transform.parent = parent;
        }
    }
}
