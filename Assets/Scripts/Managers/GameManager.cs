﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    Dictionary<int, List<PlayerController>> playerTeams = new Dictionary<int, List<PlayerController>>();

    static GameManager _instance;
    

    public static GameManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
            }
            return _instance;
        }
    }

    //Event called on End of the round passing losing team id 
    public static event Action<int> OnEndRound;


    private void Awake()
    {
        PlayerController.OnAnyDie += CheckIfRoundEnded;
    }
    
    public void SearchForPlayers() 
    {
        var foundPlayers = FindObjectsOfType<PlayerController>();

        foreach (var player in foundPlayers)
            AddPlayer(player);
    }

    public void AddPlayer(PlayerController player)
    {
        if (playerTeams.ContainsKey(player.TeamId))
        {
            playerTeams[player.TeamId].Add(player);
        }
        else 
        {
            playerTeams.Add(player.TeamId, new List<PlayerController>() { player});
        }
    }


    void CheckIfRoundEnded(PlayerController player)
    {
        Debug.Log($"Player {player.Name} from Team {player.TeamId} is Dead");

        if (playerTeams.ContainsKey(player.TeamId))
        {
            if (playerTeams[player.TeamId].All(p => p.isDead)) 
            {
                //ToDo: End Round;
                OnEndRound?.Invoke(player.TeamId);
            }
        }
    }

    private void OnDestroy()
    {
        PlayerController.OnAnyDie -= CheckIfRoundEnded;
    }
}
