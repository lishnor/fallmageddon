﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Colyseus;
using System.Threading.Tasks;
using System;
using GameDevWare.Serialization;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public enum GameMode
{
    Tutorial,
    Random,
    Join,
    Create
}

public class NetworkManager : MonoBehaviour
{
    Client _client;
    Room<RoomState> _room;
    [SerializeField]
    GameObject _player;
    PlayerNetworking _playerNetworking;
    [SerializeField]
    UIManager _uiManager;

    public GameObject _characterPrefab;
    protected IndexedDictionary<Player, GameObject> _players = new IndexedDictionary<Player, GameObject>();
    protected IndexedDictionary<string, GameObject> _playersById = new IndexedDictionary<string, GameObject>();
    GameObject[] _respawns;

    public Color[] _teamColors = new[] { new Color(1, 0, 0), new Color(0, 0.5f, 1) };

    public static GameMode gameMode = GameMode.Tutorial;
    public static string roomId;
    public static string serverAddress;

    public async void Start()
    {
        await StartGame(gameMode, roomId);
    }

    async Task StartGame(GameMode gameMode, string roomId = null)
    {
        _client = new Client("ws://"+serverAddress);
        _respawns = GameObject.FindGameObjectsWithTag("Respawn");
        _playerNetworking = _player.GetComponent<PlayerNetworking>();
        _playerNetworking.NetworkManager = this;
        if (gameMode != GameMode.Tutorial)
        {
            _playerNetworking.isLocked = true;
            var dummies = GameObject.FindGameObjectsWithTag("Dummy");
            foreach (var dummy in dummies)
            {
                dummy.SetActive(false);
            }
            try
            {
                switch (gameMode)
                {
                    case GameMode.Random:
                        _room = await _client.JoinOrCreate<RoomState>("battle");
                        break;
                    case GameMode.Create:
                        _room = await _client.Create<RoomState>("battle", new Dictionary<string, object> { { "private", true } });
                        roomId = _room.Id;
                        Debug.Log("Created room " + roomId);
                        break;
                    case GameMode.Join:
                        _room = await _client.JoinById<RoomState>(roomId);
                        break;
                }
            }
            catch (Exception ex)
            {
                Debug.Log("Join error");
                Debug.Log(ex.Message);
                return;
            }
            if (roomId != null)
            {
                _uiManager.SetLobbyId(roomId);
            }
            _room.OnMessage<string>("start", OnStart);
            _room.OnMessage<SpawnMessage>("spawn", OnSpawn);
            _room.OnMessage<int>("gameover", OnGameOver);
            _room.OnMessage<string>("dash", OnEnemyDash);
            _room.OnMessage<ShootMessage>("shoot", OnEnemyShoot);
            _room.OnMessage<HitMessage>("hit", OnPlayerHit);
            _room.State.players.OnAdd += OnEnemyAdd;
            _room.State.players.OnChange += OnEnemyChange;
            _room.State.players.OnRemove += OnEnemyRemove;
            _room.State.teams.OnChange += OnTeamChange;
            Debug.Log("Joined successfully");
        }
        else
        {
            _playerNetworking.isLocked = false;
            _playerNetworking.Spawn(_respawns[Random.Range(0, _respawns.Length)].transform);
            _player.SetActive(true);
        }
    }

    public void Send(string message, object payload)
    {
        _room?.Send(message, payload);
    }

    void OnStart(string message)
    {
        Debug.Log("Starting");
        _playerNetworking.isLocked = false;
    }

    void OnGameOver(int winningTeam)
    {
        Debug.Log("Game over. Winning team: " + winningTeam);
    }

    void OnSpawn(SpawnMessage message)
    {
        Debug.Log("Spawn" + message.teamId + message.spawnPoint);
        _playerNetworking.Spawn(_respawns[message.spawnPoint].transform);
        _player.SetActive(true);
        _playerNetworking.enabled = false;
        var cc = _player.GetComponent<ColorChanger>();
        cc.color = _teamColors[message.teamId];
        cc.SetColor();
    }

    void OnEnemyAdd(Player player, string key)
    {
        if (key != _room.SessionId)
        {
            Debug.Log("spawn" + key + " " + _room.SessionId);
            GameObject playerObject = Instantiate(_characterPrefab, new Vector3(player.x, player.y, player.z), Quaternion.identity);
            var enemyNetworking = playerObject.GetComponent<EnemyNetworking>();
            enemyNetworking.NetworkManager = this;
            enemyNetworking.ClientId = key;
            _players.Add(player, playerObject);
            _playersById.Add(key, playerObject);
            var cc = playerObject.GetComponent<ColorChanger>();
            cc.color = _teamColors[player.teamId];
            cc.SetColor();
        }
    }

    void OnEnemyChange(Player player, string key)
    {
        if (key != _room.SessionId)
        {
            Debug.Log("move");
            GameObject playerObject;
            if (_players.TryGetValue(player, out playerObject))
            {
                var position = new Vector3(player.x, player.y, player.z);
                var rotation = new Quaternion(player.a, player.b, player.c, player.d);
                var enemyNetworking = playerObject.GetComponent<EnemyNetworking>();
                enemyNetworking.Move(position, rotation, player.grounded, player.velocity);
                enemyNetworking.ChangeWeapon(player.weaponType);
            }
        }
    }

    void OnEnemyRemove(Player player, string key)
    {
        if (key != _room.SessionId)
        {
            Debug.Log("remove");
            GameObject playerObject;
            if (_players.TryGetValue(player, out playerObject))
            {
                Destroy(playerObject);
                _players.Remove(player);
            }
        }
    }

    void OnEnemyDash(string id)
    {
        if (id != _room.SessionId)
        {
            Debug.Log("dash");
            GameObject playerObject;
            if (_playersById.TryGetValue(id, out playerObject))
            {
                playerObject.GetComponent<EnemyNetworking>().Dash();
            }
        }
    }

    void OnEnemyShoot(ShootMessage m)
    {
        if (m.clientId != _room.SessionId)
        {
            Debug.Log("shoot");
            GameObject playerObject;
            if (_playersById.TryGetValue(m.clientId, out playerObject))
            {
                playerObject.GetComponent<EnemyNetworking>().Shoot(new Quaternion(m.a, m.b, m.c, m.d));
            }
        }
    }

    void OnPlayerHit(HitMessage m)
    {
        if (m.clientId == _room.SessionId)
        {
            Debug.Log("hit");
            _player.GetComponent<PlayerNetworking>().GetHit(m.damage, new Vector3(m.x, m.y, m.z));
        }
    }

    public void OnPlayerDied(object messagePayload)
    {
        if (gameMode == GameMode.Tutorial)
        {
            SceneManager.LoadScene("SampleScene");
        }
        else
        {
            Send("die", messagePayload);
        }
    }

    void OnTeamChange(Team team, string key)
    {
        _uiManager.SetTeamInfo(int.Parse(key), team.score, team.playersAliveCount);
    }
    
    async Task OnDestroy()
    {
        await _room.Leave();
    }

}

