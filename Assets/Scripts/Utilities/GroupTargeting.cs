﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System.Linq;

public class GroupTargeting : MonoBehaviour
{
    CinemachineTargetGroup targetGroup;
    public Transform sessionPlayer;
    [SerializeField] float radius;
    [SerializeField] LayerMask mask;
    Collider[] targets;
    [SerializeField] bool lookForNearby = true;
    WaitForSeconds wait = new WaitForSeconds(0.1f);

    void Start()
    {
        targetGroup = GetComponent<CinemachineTargetGroup>();
        targets = Caster();
        targetGroup.RemoveMember(sessionPlayer);
        foreach (var a in targets) targetGroup.AddMember(a.transform, 1f, 1f);
        StartCoroutine(ChceckForNearbyCameraTargets());
    }

    Collider[] Caster()
    {
        return Physics.OverlapSphere(sessionPlayer.position, radius, mask,QueryTriggerInteraction.Ignore);
    }

    IEnumerator ChceckForNearbyCameraTargets()
    {
        while (lookForNearby) 
        {
            var newCast = Caster();
            var toRemove = targets.Except(newCast);
            var toAdd = newCast.Except(targets);
            foreach (var r in toRemove) targetGroup.RemoveMember(r.transform);
            foreach (var a in toAdd) targetGroup.AddMember(a.transform, 1f, 1f);
            targets = newCast;
            yield return wait;
        }
    }
}

