﻿using System;
using System.Collections;
using UnityEngine;

public class PoolObject : MonoBehaviour
{
    [HideInInspector]public int poolKey;
    public Action OnDestroy;

    public virtual void OnObjectReuse() 
    {
        
    }

    protected void Destroy() 
    {
        PoolManager.instance.EnqueueObject(this);
        gameObject.SetActive(false);
        OnDestroy?.Invoke();
    }
}
