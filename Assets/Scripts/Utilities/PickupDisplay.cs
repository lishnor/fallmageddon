﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PickupDisplay : MonoBehaviour
{
    [SerializeField] float targetScale;

    void Start()
    {
        transform.DOLocalRotate(new Vector3(0.0f, 90f, 0.0f), 1.0f).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear).SetRelative();
        transform.DOLocalMoveY(0.6f, 1f).SetLoops(-1,LoopType.Yoyo).SetEase(Ease.InOutSine);
        transform.DOScale(targetScale,1f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
    }

}
