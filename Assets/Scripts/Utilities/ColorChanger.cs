﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Sets visual representaion for player
/// </summary>
public class ColorChanger : MonoBehaviour
{
    public Color color;

    [SerializeField] SkinnedMeshRenderer playerMeshRenderer;
    [SerializeField] string playerMaterialHueParameter = "_HueShift";
    private MaterialPropertyBlock playerPropertyBlock;

    [SerializeField] MeshRenderer circleMeshRenderer;
    [SerializeField] string circleMaterialColorParameter = "_Color";
    private MaterialPropertyBlock circlePropertyBlock;

    [SerializeField] UnityEngine.UI.Image crosshair;

    private void Awake()
    {
        playerPropertyBlock = new MaterialPropertyBlock();
        circlePropertyBlock = new MaterialPropertyBlock();
        SetColor();
    }

    [ContextMenu("SetColor")]
    public void SetColor()
    {


        float H,S,V;
        Color.RGBToHSV(color, out H,out S, out V);

        if (playerMeshRenderer != null) 
        {
            playerMeshRenderer.GetPropertyBlock(playerPropertyBlock);
            playerPropertyBlock.SetFloat(playerMaterialHueParameter, H * 360f);
            playerMeshRenderer.SetPropertyBlock(playerPropertyBlock);
        }

        if(circleMeshRenderer != null)
        {
            circleMeshRenderer.GetPropertyBlock(circlePropertyBlock);
            circlePropertyBlock.SetColor(circleMaterialColorParameter, color);
            circleMeshRenderer.SetPropertyBlock(circlePropertyBlock);
        }
        
        if(crosshair != null) crosshair.color = Color.HSVToRGB(H,S-0.05f,V);
    }
}
