﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleIndicatorSetter : MonoBehaviour
{
    [SerializeField] Transform cirleIndicator;

    void Start()
    {
        
    }

    void Update()
    {
        int layerMask = 1 << 9;
        layerMask = ~layerMask;

        RaycastHit hit;
        if (Physics.Raycast(transform.position+ Vector3.up * 0.1f, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity, layerMask))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            cirleIndicator.position = hit.point + Vector3.up * 0.1f;
        }
    }
}
