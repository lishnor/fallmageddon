﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickup : MonoBehaviour
{
    [SerializeField] int gun;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) 
        {
            other.gameObject.GetComponent<PlayerController>().GunHolster.activeGunId = gun;
            other.gameObject.GetComponent<PlayerNetworking>().ChangeWeapon(gun);
            FindObjectOfType<AudioManager>().Play("PickUp");
        }
    }
}
